module.exports = function(grunt) {
  'use strict';

  // autoload tasks
  require('load-grunt-tasks')(grunt);

  // show elapsed time at the end
  require('time-grunt')(grunt);

  // load config files
  // require('load-grunt-config')(grunt);

  grunt.initConfig({

    // Project settings
    config: {
      app: 'app',
      dist: 'dist'
    },

    // Livereload
    watch: {
      options: {
        livereload: true
      },
      less: {
        files: ['<%= config.app %>/less/{,*/}*.less'],
        tasks: ['less:dev']
      },
      js: {
        files: ['<%= config.app %>/js/{,*/}*.js']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.app %>{,*/}*.html',
          '.tmp/css/{,*/}*.css',
          '.tmp/js/{,*/}*.js',
          '<%= config.app %>/img/{,*/}*.{gif,jpeg,jpg,png,svg,webp}'
        ]
      }
    }, // watch end

    // Connect
    connect: {
      options: {
        port: 9000,
        livereload: 35729,
        hostname: 'localhost' // 0.0.0.0 to access the server from outside
      },
      livereload: {
        options: {
          open: true,
          base: [
            '.tmp',
            '<%= config.app %>'
          ]
        }
      },
      dist: {
        options: {
          open: true,
          base: [
            '<%= config.app %>',
            '.tmp'
          ]
        }
      }
    },

    // Clean files and folders
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/*',
            '!/<%= config.dist %>/.git'
          ]
        }]
      },
      server: '.tmp'
    },

    // JavaScript Tool
    jshint: {
      all: [
        'Gruntfile.js',
        ['<%= config.app %>/js/*.js']
      ]
    },

    // JavaScript Compressor
    uglify: {
      dev: {
        src: ['<%= config.app %>/js/*.js'],
        dest: '<%= config.app %>/js/scripts.min.js'
      },
      production: {
        src: ['<%= config.app %>/js/*.js'],
        dest: '<%= config.dist %>/js/scripts.min.js'
      }
    },

    // Minify images
    imagemin: {
      options: {
        optimizationLevel: 3,
        progressive: true,
        interlaced: true,
        pngquant: true
      },
      dynamic: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/img',
          src: ['**/*.{png,jpg,gif}'],
          dest: '<%= config.dist %>/img'
        }]
      },
      content: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/pic',
          src: ['**/*.{png,jpg,gif}'],
          dest: '<%= config.dist %>/pic'
        }]
      }
    },

    // Minify HTML
    htmlmin: {
      dist: {
        options: {},
        files: [{
          expand: true,
          cwd: '<%= config.app %>',
          src: '*.html',
          dest: '<%= config.dist %>'
        }]
      }
    },

    // Sorting CSS properties (see .csscomb.json)
    csscomb: {
      dynamic_mappings: {
        expand: true,
        cwd: '<%= config.app %>/css/',
        src: ['*.css', '*.less'],
        dest: '<%= config.app %>/css/',
        ext: '.less'
      }
    },

    // Add vendor's prefixes
    autoprefixer: {
      options: {
        cascade: true,
        browsers: ['last 2 versions', 'ie 9']
      },
      all: {
        expand: true,
        flatten: true,
        src: ['<%= config.dist %>/css/*.css'],
        dest: '<%= config.dist %>/css/'
      }
    },

    // Copy files and folders
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '*.{ico,png,txt,xml,html,md}',
            'font/{,*/}*.*'
          ]
        }]
      }
    },

    // Run grunt tasks concurrently
    concurrent: {
      dev: [
        'less:dev'
      ],
      dist: [
        'less:dist',
        'imagemin'
      ]
    },

    // Less
    less: {
      dev: {
        options: {
          sourceMap: true
        },
        files: {
          '.tmp/css/style.css': '<%= config.app %>/less/style.less'
        }
      },
      dist: {
        options: {
          compress: true,
          report: true
        },
        files: {
          'dist/css/style.css':'<%= config.app %>/less/style.less'
        }
      }
    },

    // Sprite
    sprite: {
      all: {
        // Specify algorithm (top-down, left-right, diagonal, alt-diagonal, binary-tree)
        'algorithm': 'top-down',
        src: '<%= config.app %>/sprites/*.png',
        destImg: '<%= config.app %>/sprites/sprite.png',
        destCSS: '<%= config.app %>/sprites/sprite.less'
      }
    }
  });
  // Tasks.
  grunt.registerTask('default', ['jshint', 'build']);

  grunt.registerTask('build', [
    'uglify:production',
    'autoprefixer:all',
    'csscomb',
    'copy:dist',
    'less:dist',
    'imagemin:dynamic',
    'imagemin:content',
    'htmlmin'
  ]);

  grunt.registerTask('test', [
    'jshint:all',
    'autoprefixer:all',
    'csscomb'
  ]);

  grunt.registerTask('serve', function (target) {
    if (target === 'build') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      // 'uglify:dev',
      'concurrent:dev',
      'csscomb',
      'connect:livereload',
      'watch'
    ]);
  });
};
