# Grunt Settings

Small css-framework who uses [Grunt](https://github.com/gruntjs/grunt).

## What grunt settings include?

Grunt settings will let you to manage your css and javascript files and start your new project faster.
It includes some features, which will be only expand in the future.

### Feathures

- Responsive CSS menu
- Grunt plugins:
  - LESS
  - Autoprefixer
  - Csscomb
  - Sprite Generator
  - Jshint
  - Uglify
  - Livereload
  - Imagemin
  - Htmlmin


## How to use?

Make the follow steps:

```console
# emerge -pav net-libs/nodejs
# npm install -g grunt-cli
$ git clone git@bitbucket.org:redVi/grunt-settings.git
$ cd genesis && nmp install
$ grunt serve
```

Enjoy!

## Supported browsers

browser  | version
-------- | -------------
Firefox  | >=32
Chrome   | >=37
Opera    | >=25
IE       | >=9
